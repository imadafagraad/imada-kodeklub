# IMADA Kodeklub hjemmeside

Start med at kopier `.env.sample` til `.env`

### Kør med Flask
```shell
# Lav et Python venv
python3 -m venv .venv
source .venv/bin/activate

# Installér app dependencies
python install -r requirements.txt
# Tillader Flask at læse .env
pip install python-dotenv

FLASK_ENV=development FLASK_APP=app/main.py flask run
```

### Kør med Docker
Kan også køre via Docker med. Fordelen med at køre via rent Flask er dog at den genindlæser ændringer automatisk
```shell
docker-compose up -d
```