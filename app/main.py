import os
import json
import secrets
from datetime import date
from urllib.parse import urlencode

from flask import Flask
from flask import render_template, redirect, abort, request, url_for
from flaskext.markdown import Markdown

import requests

app = Flask(__name__)
Markdown(app)

app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get(
    'IKK_DB_URI', 'sqlite:///kodeklub.db')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

app.config['DOMAIN'] = os.environ.get(
    'IKK_DOMAIN', 'https://kodeklub.imadafagraad.dk')
app.config['GITHUB_CLIENT_ID'] = os.environ.get('IKK_GITHUB_CLIENT_ID', None)
app.config['GITLAB_CLIENT_ID'] = os.environ.get('IKK_GITLAB_CLIENT_ID', None)
app.config['IMADA_GIT_CLIENT_ID'] = os.environ.get(
    'IKK_IMADA_GIT_CLIENT_ID', None)
app.config['IMADA_GIT_CLIENT_SECRET'] = os.environ.get(
    'IKK_IMADA_GIT_CLIENT_SECRET', None)

current_assignment = {
    'id': 1,
    'title': 'Number scrabble',
    'short_description': 'Vi starter småt med et lille spil',
    'start': date(2021, 2, 10),
    'end': date(2021, 2, 24),
    'description': """
Number scrabble er et spil man spiller en mod en. Man skiftes til at vælge tal fra 1 til 9, hvor hvert tal kun må vælges en gang. Målet er at kunne lægge tre af sine tal sammen til 15. 

Måske du før har implementeret spillet eller meget af det, da det er en [isomorfi](https://en.wikipedia.org/wiki/Isomorphism) til kryds og bolle. Spillet kan nemlig repræsenteres som en [magisk kvadrat](https://en.wikipedia.org/wiki/Magic_square).

Når man implementerer et spil er det en god idé med en måde at spille det på. Her ville det være oplagt at kunne spille mod computeren. Dette udgør kernen i denne opgave: implementere number scrabble og en strategi til spillet.

Det kan være at dette er en opgave der er meget hurtig at løse, men det giver vel mere tid til at udvide sin løsning. Det mest oplagte vil være en pæn brugeroverflade til spillet. Ville det fungere med andre tal end fra 1 til 9 og summen på 15? Hvad med at vise det som en magisk kvadrat?
    """.strip()
}


@app.route('/')
def index():
    return render_template('index.html', current_assignment=current_assignment)


@app.route('/assignments/')
def list_assignments():
    return 'Her er alle opgaver'


@app.route('/assignments/<id>')
def view_assignment(id):
    return render_template('assignment.html', assignment=current_assignment)


@app.route('/login/')
def login():
    return render_template('login.html')


def imadagit_redirect_uri():
    domain = app.config['DOMAIN']
    return f"{domain}/oauth/imadagit"


@app.route('/login/imadagit/')
def login_imadagit():
    client_id = app.config['IMADA_GIT_CLIENT_ID']
    redirect_uri = imadagit_redirect_uri()

    if client_id is None or redirect_uri is None:
        error = 'Intern serverfejl'
        return render_template('login.html', error=error), 500

    token = secrets.token_urlsafe(24)

    state = {
        'token': token
    }

    url_args = {
        'client_id': client_id,
        'redirect_uri': redirect_uri,
        'state': json.dumps(state),
        'response_type': 'code'
    }

    return redirect(f"https://git.imada.sdu.dk/login/oauth/authorize?{urlencode(url_args)}")


@app.route('/oauth/imadagit')
def oauth_imadagit():
    code = request.args.get('code', None)
    state = request.args.get('state', None)

    redirect_url = url_for('login')

    if code is None or state is None:
        return render_template('oauth.html',
                               message='Der skete en fejl',
                               redirect_url=redirect_url), 400

    client_id = app.config['IMADA_GIT_CLIENT_ID']
    client_secret = app.config['IMADA_GIT_CLIENT_SECRET']
    redirect_uri = imadagit_redirect_uri()

    r = requests.post('https://git.imada.sdu.dk/login/oauth/access_token', data={
        'client_id': client_id,
        'client_secret': client_secret,
        'code': code,
        'grant_type': 'authorization_code',
        'redirect_uri': redirect_uri
    })

    access_token_json = r.json()
    access_token = access_token_json.access_token

    user_info_request = requests.post('https://git.imada.sdu.dk/api/v1/user', params={
        'token': access_token
    })

    user_info = user_info_request.json()

    return render_template('profile.html', user=user_info)
